#!/bin/bash

## Collect all links to most pertinent data for transfer from HPC.
##
##     Collects data into data type folders, using symbolic links for storing
## representation. Data can then be easily transfered from HPC to local for
## ease of computation.
##
## This script requires the following:
## - CommonSense v1.2

## Usage statement
if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
    echo "Usage: $0 <INPUT_DIR> <OUTPUT_DIR>"
    echo ""
    echo "  Collects data into data type folders using symbolic links for storing"
    echo "  most pertinent data which can then be transferred from HPC to local."
    echo ""
    echo "  <INPUT_DIR>   super directory which holds collection of sample folders"
    echo "  <OUTPUT_DIR>  directory which will hold the links"
    echo ""
    exit 1
fi


## Preset parameters
INDIR=$(readlink -f $1)
OUTDIR=$(readlink -f $2)

echo ""
echo "Beginning collection process on from $INDIR .."
echo ".. Collecting: "
echo ".. - ChIP-seq: bams, bigwigs, macs2, bcp, music"
echo ".. - RNA-seq: kallisto"
echo ""
echo "Placing in: $OUTDIR"
echo ""

mkdir -p $OUTDIR
cd $OUTDIR
mkdir -p bam bigwig macs2 bcp music kallisto

for I in $(ls -d $INDIR/*); do 
    
    ## Easy cases: Sample ID already embedded in file name
    cd $OUTDIR/bam
    if [[ -e $I/bam_clean ]]; then
	for J in $(ls $I/bam_clean/*.bam*); do 
	    ln -s $(readlink -f $J) .
	done
    fi

    cd $OUTDIR/bigwig
    if [[ -e $I/bigwig ]]; then
	for J in $(ls $I/bigwig/*.bw); do 
	    ln -s $(readlink -f $J) .
	done
    fi

    cd $OUTDIR/macs2
    if [[ -e $I/macs2 ]]; then
	for J in $(ls $I/macs2/*Peak); do 
	    ln -s $(readlink -f $J) .
	done
    fi

    cd $OUTDIR/bcp
    if [[ -e $I/bcp ]]; then
	for J in $(ls $I/bcp/*bcp.bed); do 
	    ln -s $(readlink -f $J) .
	done
    fi

    ## Need to embed sample ID as prefix
    cd $OUTDIR/music
    if [[ -e $I/music ]]; then
	for J in $(ls $I/music/chip/ER/*.bed); do 
	    ln -s $(readlink -f $J) $(basename $I)_music_$(basename $J)
	done
    fi
    
    ## Need to link over whole folder
    cd $OUTDIR/kallisto
    if [[ -e $I/kallisto ]]; then
	for J in $(ls -d $I/kallisto); do 
	    ln -s $(readlink -f $J) $(basename $I)_kallisto
	done
    fi

done    

