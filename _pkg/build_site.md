Navigate to `_pkg` to build website:

```{r}
devtools::document()
pkgdown::build_site(examples = FALSE, preview = FALSE, path = "../docs")
```

Replace all `()` with `.sh` to correctly suffix the scripts in reference files by running:

`grep -rl "()" docs/reference | xargs sed -i "" 's/()/.sh/g' ## or nav to docs/reference folder`

Then simply navigate to docs and run:

`aero deploy`
