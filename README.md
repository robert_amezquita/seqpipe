# seqpipe

## Overview 

[`seqpipe`](https://seqpipe.aerobatic.io) is a collection of scripts which together form a pipeline for the processing of sequencing data, specifically regarding the processing primarily of ChIP-seq, ATAC-seq, with a couple additional RNA-seq specific scripts.

If here courtesy of Bitbucket or source code, check out the docs online at: [`seqpipe`](https://seqpipe.aerobatic.io), and the package [vignette](https://seqpipe.aerobatic.io/articles/seq-flow-tutorial).

Where `seqpipe` excels is in separating processing into two distinct phases:

* *Per Sample Processing* - each sample is treated independently, and processing can occur in parallel
* *Replicate-aware Processing* - samples are combined into condition-specific folders such that interdependent samples can be processed together. This is particularly important for ChIP-seq where samples are paired with cognate inputs, and additionally for methods which are designed to consider replicates

Each script (see the Reference page) takes on a specific filetype or step of the process, and is labeled chronologically in alphabetical order, designed to run from A through Z. 

## Vignette 

Check out the [vignette](https://seqpipe.aerobatic.io/articles/seq-flow-tutorial) to learn more on the design and execution of the `seqpipe`.

## Usage

For inspecting source code and downloading the latest pipeline package, navigate to the [Downloads](https://bitbucket.org/robert_amezquita/seqpipe/downloads) courtesy of Bitbucket.

---

#### Contact

* Robert A. Amezquita (aut, cre, cph): [robert.amezquita@yale.edu](robert.amezquita@yale.edu)
