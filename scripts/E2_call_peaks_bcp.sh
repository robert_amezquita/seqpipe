#!/bin/bash

## Calling Peaks with BCP and database table
##
##     Using an alternative peak caller to call enrichments in ChIP-seq data using BCP
##
## This script requires the following:
## - BCP_v1.1

## Usage statement
if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
    echo "Usage: $0 <PREFIX> <TABLE>"
    echo ""
    echo "  Performs peak-calling on a given sample directory deriving sample:input"
    echo "relationships from a database table using BCP peak caller."
    echo ""
    echo "  <COND>     condition directory from a repdat folder where sample and inputs"
    echo "             are organized based on condition."
    echo "  <TABLE>    tab-delimited table specifying the CONDITION_ID, SAMPLE_ID,"
    echo "             and INPUT_ID as columns, and a fourth TYPE column for punctate"
    echo "             vs. broad peak-calling, with any additional columns being"
    echo "             ignored. The first line (header) will be ignored."
    echo ""
    exit 1
fi


## Preset parameters
## 0. Read in Parameters
COND=$(readlink -f $1)
TAB=$(readlink -f $2)

## 0. Echo parameters back
echo ""
echo "Beginning to process your ChIP-seq/ATAC-seq data using BCP.."
echo ""
echo "Working on samples in: $COND"
echo ""


## 1. Routine for all samples within condition

## Per sample within the sample directory
for I in $(ls -d $COND/samples/*); do 
    SDIR=$(readlink -f $I)
    PREFIX=$(basename $I)

    ## Reading database table for parameters
    SAMPLEID=$(grep $PREFIX $TAB | awk '{print $2}')
    INPUTID=$(grep $PREFIX $TAB | awk '{print $3}')
    # PEAKTYPE=$(grep $PREFIX $TAB | awk '{print $4}')
    # SEQTYPE=$(grep $PREFIX $TAB | awk '{print $5}')

    IDIR=$(readlink -f $COND/inputs/$INPUTID)
    IPREFIX=$(basename $IDIR)

    echo ""
    echo ".. Calling peaks for: $PREFIX"


    ## If INPUT is NA (ATAC-seq), go to next sample
    if [ $INPUTID == "NA" ]; then
	echo ".. Input is NA, skipping.."
	continue
    fi

    ## Make directories 
    echo ".. Make directories.."
    mkdir -p $SDIR/bcp $IDIR/bcp

    ## Sample processing - convert bam to bed
    echo ".. Sample processing.."
    if [ ! -s $SDIR/bcp/${PREFIX}.bed ]; then
    echo ".. Convert sample from bam to bed.."
	bedtools bamtobed -i $SDIR/bam_clean/${PREFIX}.bam > $SDIR/bcp/${PREFIX}.bed
    fi


    if [ ! -s $SDIR/bcp/${PREFIX}.bed ]; then
    echo ".. Convert input from bam to bed.."
	bedtools bamtobed -i $IDIR/bam_clean/${IPREFIX}.bam > $IDIR/bcp/${IPREFIX}.bed
    fi
    
    ## Run BCP
    echo ".. Running BCP.."
    BCP_HM -1 $SDIR/bcp/${PREFIX}.bed -2 $IDIR/bcp/${IPREFIX}.bed -f 200 -w 200 -p 0.001 -3 $SDIR/bcp/${PREFIX}_bcp.bed
    
    echo ""
done


echo ""
echo "Peak-calling completed."
echo ""
