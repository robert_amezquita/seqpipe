#!/bin/bash

## Template for Creating Scripts
##
##     This is where the description would go.
##
## This script requires the following:
## - CommonSense v1.2

## Usage statement
if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
    echo "Usage: $0 <PREFIX> <THREADS>"
    echo ""
    echo "  Brief description."
    echo ""
    echo "  <PREFIX>   sample directory where fastq files are located in a"
    echo "             directory fastq (thanks to 00.get-samples.sh)      "
    echo "  <THREADS>  allocated threads for the job; note that memory amt"
    echo "             proportional to number of threads / 20 * 128G (Ruddle)"
    echo "             which ends up being about 6GB per thread"
    echo ""
    exit 1
fi


## Preset parameters
BLACKLIST=/home/ra364/Reference/Mus_musculus/Ensembl/GRCm38/Annotation/Genes/mm10_blacklist_ENSEMBL.bed
TWOBIT=/home/ra364/Reference/Mus_musculus/Ensembl/GRCm38/Sequence/2bit/genome.2bit 
MEMPERTHREAD=6                 # GB; 128 GB of RAM / 20 cores per node
EFFGENOMESIZE=2150570000       # Effective genome size (mm10) [hg19 = 2451960000]
REPMASK=/home/ra364/Reference/Mus_musculus/Ensembl/GRCm38/Annotation/Genes/rmsk_ens.bed
CHRSIZES=~/Reference/Mus_musculus/Ensembl/GRCm38/Annotation/Genes/ChromInfo.Ens.txt 


BLACKLIST=/home/ra364/Reference/Mus_musculus/Ensembl/GRCm38/Annotation/Genes/mm10_blacklist.bed    ## UCSC chr
TWOBIT=/home/ra364/Reference/Mus_musculus/Ensembl/GRCm38/Annotation/Sequence/2bit_UCSC/genome.2bit ## UCSC chr


## 0. Read in Parameters


## 0. Echo parameters back
echo ""
echo "Beginning to process your paired-end ChIP-seq/ATAC-seq data.."
echo ".. The Bowtie2 Index is: $GENOMEDIR"
echo ".. Memory per thread is: $MEMPERTHREAD GB"
echo ".. Cutting the TruSeq Adapters (see cutadapt documentation)"
echo ".. Filtering reads from the blacklist: $BLACKLIST"
echo ""
echo "Working on: $SDIR"
echo ".. Using prefix: $PREFIX"
echo ".. Using $THREADS threads"
echo ""


## 1. Part 1.
