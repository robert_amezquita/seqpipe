#!/bin/bash

## Create Normalized bigwig Tracks
##
##     This runs on processed alignment files (bam) to create normalized
## visualization tracks, primarily for use in IGV/UCSC Genome Browser. Note
## that this script requires a preset effective genome size and a genome
## sequence file in 2bit format (see UCSC utility faToTwoBit for conversion of
## .fa files). 
##
## This script requires the following:
## - deepTools v2.2.4

## Usage statement
if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
    echo "Usage: $0 <PREFIX> <THREADS>"
    echo ""
    echo "  Runs deepTools2 to normalize processed bam files to Reads per "
    echo "Genomic Content (RPGC) within a given sample directory."
    echo ""
    echo "  <PREFIX>   sample directory where fastq files are located in a"
    echo "             directory fastq (thanks to 00.get-samples.sh)      "
    echo "  <THREADS>  allocated threads for the job; note that memory amt"
    echo "             proportional to number of threads / 20 * 128G (Ruddle)"
    echo "             which ends up being about 6GB per thread"
    echo ""
    exit 1
fi


## Preset parameters
MEMPERTHREAD=6                 # GB; 128 GB of RAM / 20 cores per node
EFFGENOMESIZE=2150570000       # Effective genome size (mm10) [hg19 = 2451960000]
BLACKLIST=/home/ra364/Reference/Mus_musculus/Ensembl/GRCm38/Annotation/Genes/mm10_blacklist.bed    ## UCSC chr

## 0. Read in Parameters
SDIR=$(readlink -f $1)
PREFIX=$(basename $SDIR)
THREADS=$2


## 0. Echo parameters back
echo ""
echo "Beginning to process your bam files for conversion to bigwig.."
echo ".. Memory per thread is: $MEMPERTHREAD GB"
echo ".. Filtering reads from the blacklist: $BLACKLIST"
echo ".. Effective Genome Size (mm10): $EFFGENOMESIZE"
echo ""
echo "Working on: $SDIR"
echo ".. Using prefix: $PREFIX"
echo ".. Using $THREADS threads"
echo ""

# echo ".. Loading module Langs/Python/2.7.11_libs.."
# module load Langs/Python/2.7.11_libs


## 1. Part 1.
echo "1. Calculating coverage for bigwig.."
mkdir -p $SDIR/bigwig
cd $SDIR

bamCoverage \
    -b $SDIR/bam_clean/${PREFIX}.bam \
    -o $SDIR/bigwig/${PREFIX}.bw \
    --outFileFormat=bigwig \
    --blackListFileName $BLACKLIST \
    --numberOfProcessors=$THREADS \
    --binSize=10 \
    --smoothLength=30 \
    --normalizeTo1x $EFFGENOMESIZE \
    --extendReads=300 \
    --ignoreDuplicates
    
## do not set ignoreDups when using GC bias correction
##    -b $SDIR/correctGCBias/${PREFIX}_gc.bam \


echo ""
echo "Coverage calculation complete."
echo ""



#
# SDIR=/ycga-ba/data2/kaech/OpenSeq/test/pe_test/Sample_SG_34_001 ## for testing
# SDIR=/ycga-ba/data2/kaech/OpenSeq/test/se_test/Sample_TG_65     ## for testing

