#!/bin/bash

## Automagic Track Hub writer for input data
##
##     This script automagically creates a UCSC Genome Browser trackhub 
## using the replicate aware structure created by 03.combine_conditions.sh 
## to make bigwig containers based on condition, placing relevant samples 
## into said container. This script solely writes the hub for input data,
## since typically this is not of interest for hubs.
##
## This script requires the following:
## - CommonSense v1.2

## Usage statement
if [ $# -eq 0 ] || [ $1 == "-h" ] || [ $1 == "--help" ]; then
    echo "Usage: $0 <REPDAT> <HUB>"
    echo ""
    echo "  This script automagically creates a UCSC Genome Browser compatible"
    echo "trackhub using a replicate-aware directory structure to make bigwig"
    echo "containers for groups of samples. This script is solely writes tracks"
    echo "for input data."
    echo ""
    echo "  <REPDAT>   replicate-aware directory with samples organized by"
    echo "             condition, e.g. condition > samples > sample1, sample2"
    echo "             and inputs > input1, input2; these individual sample/input"
    echo "             directories must each have a directory named bigwig with"
    echo "             a UCSC Genome Browser compatible bigwig file"
    echo "  <HUB>      directory where the trackhub will be written"
    echo ""
    exit 1
fi

## 0. Read in Parameters
REPDAT=$(readlink -f $1)
HUB=$(readlink -f $2)
ORGANISM=mm10

## 0. Echo parameters back
echo ""
echo "Beginning to write your trackhub.."
echo ".. Assuming this is $ORGANISM .."
echo ""


## 1. Part 1.
mkdir -p $HUB
mkdir -p $HUB/$ORGANISM/bigwig

echo "" > $HUB/mm10/trackDb.txt
I=1
for CDIR in $(ls -d $REPDAT/*); do 
    ## Check if input dir is empty; if so, skip to next dir
    if [ ! "$(ls -A $CDIR/inputs)" ]; then
	echo "$(basename $CDIR) input directory is empty; skipping.."
	continue
    fi

    CONDITION=$(basename $CDIR)_Input
    mkdir -p $HUB/$ORGANISM/bigwig/$CONDITION
    echo "Writing $CONDITION .."

    echo ""                                                          >> $HUB/$ORGANISM/trackDb.txt
    echo "## ======================================================" >> $HUB/$ORGANISM/trackDb.txt
    echo "## $CONDITION"                                             >> $HUB/$ORGANISM/trackDb.txt
    echo "## ======================================================" >> $HUB/$ORGANISM/trackDb.txt    
    echo ""                                                          >> $HUB/$ORGANISM/trackDb.txt
    echo "track $CONDITION"                                          >> $HUB/$ORGANISM/trackDb.txt
    echo "container multiWig"                                        >> $HUB/$ORGANISM/trackDb.txt
    echo "shortLabel $CONDITION"                                     >> $HUB/$ORGANISM/trackDb.txt
    echo "longLabel $CONDITION"                                      >> $HUB/$ORGANISM/trackDb.txt
    echo "priority $I"                                               >> $HUB/$ORGANISM/trackDb.txt
    echo "configurable on"                                           >> $HUB/$ORGANISM/trackDb.txt
    echo "visibility full"                                           >> $HUB/$ORGANISM/trackDb.txt
    echo "maxHeightPixels 100:32:8"                                  >> $HUB/$ORGANISM/trackDb.txt
    echo "viewLimits 0:50"                                           >> $HUB/$ORGANISM/trackDb.txt
    echo "viewLimitsMax 0:1000"                                      >> $HUB/$ORGANISM/trackDb.txt
    echo "type bigWig 0 100"                                         >> $HUB/$ORGANISM/trackDb.txt
    echo "altColor 153,0,153"                                        >> $HUB/$ORGANISM/trackDb.txt
    echo "autoScale on"                                              >> $HUB/$ORGANISM/trackDb.txt
    echo "alwaysZero on"                                             >> $HUB/$ORGANISM/trackDb.txt
    echo "aggregate transparentOverlay"                              >> $HUB/$ORGANISM/trackDb.txt
    echo "showSubtrackColorOnUI on"                                  >> $HUB/$ORGANISM/trackDb.txt
    echo ""                                                          >> $HUB/$ORGANISM/trackDb.txt
    
    J=1
    for IDIR in $(ls -d $CDIR/inputs/*); do 
	INPUT=$(basename $IDIR)
	echo ".. Writing $INPUT .."

	## Write trackDb lines
	echo "  track $INPUT"                                       >> $HUB/$ORGANISM/trackDb.txt
	echo "  shortLabel $INPUT"                                  >> $HUB/$ORGANISM/trackDb.txt
	echo "  longLabel $INPUT"                                   >> $HUB/$ORGANISM/trackDb.txt
	echo "  parent $CONDITION"                                   >> $HUB/$ORGANISM/trackDb.txt
	echo "  color 153,0,153"                                     >> $HUB/$ORGANISM/trackDb.txt
	echo "  priority $J"                                         >> $HUB/$ORGANISM/trackDb.txt
	echo "  bigDataUrl bigwig/$CONDITION/${INPUT}.bw"           >> $HUB/$ORGANISM/trackDb.txt
	echo "  type bigWig 0 100"                                   >> $HUB/$ORGANISM/trackDb.txt
	echo "  windowingFunction maximum"                           >> $HUB/$ORGANISM/trackDb.txt
	echo "  smoothingWindow off"                                 >> $HUB/$ORGANISM/trackDb.txt
	echo "  visibility full"                                     >> $HUB/$ORGANISM/trackDb.txt
	echo "  negateValues off"                                    >> $HUB/$ORGANISM/trackDb.txt
	echo ""                                                      >> $HUB/$ORGANISM/trackDb.txt

	## Link in bigwig file
	cd $HUB/$ORGANISM/bigwig/$CONDITION 
	ln -sf $IDIR/bigwig/${INPUT}.bw .

	J=$(($J + 1))
    done

    I=$(($I + 1))
done

echo ""
echo "Writing your hub file.."
printf \
"hub $(basename $HUB)
shortLabel $(basename $HUB)
longLabel $(basename $HUB)
genomesFile genomes.txt
email first.last@yale.edu" > $HUB/hub.txt

echo "Writing your genomes file.."
printf \
"genome $ORGANISM
trackDb $ORGANISM/trackDb.txt
description description
organism $ORGANISM" > $HUB/genomes.txt


echo ""
echo "Don't forget to edit your hub and genomes files to complete setup."
echo ""
echo "The hub has been created."
echo ""
